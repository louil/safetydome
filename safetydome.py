#! /usr/bin/env python3

import psycopg2
con = psycopg2.connect(database='safetydome', user='flask', password='flask', host='localhost')
cur = con.cursor()

from flask import Flask, abort, request, render_template

app = Flask(__name__)

#Created class to be used in combatants function
class Object():
	def __init__(self, ids, name, species):
		self.id = ids
		self.name = name
		self.species = species

#Cretaed class to be used in battle function
class Fight():
	def __init__(self, one_id, two_id, one_name, two_name, winner, ids):
		self.one_id = one_id
		self.two_id = two_id
		self.one_name = one_name
		self.two_name = two_name
		self.winner = winner
		self.id = ids

#After being run allows index.html to be displayed on screen
@app.route('/')
def index():
	return render_template('index.html')

#After being ran allows for combatant_id information to be displayed when the id is clicked on
@app.route('/combatant/<identity>')
def combatant_id(identity=None):
	cur.execute('''select combatant.id, combatant.name, species.type, species.name, species.base_atk, species.base_dfn, species.base_hp from combatant, species
				where combatant.species_id=species.id;''')
	result = list(cur.fetchall())
	for row in result:
		if row[0] == int(identity):
			return render_template('combatant_id.html', row=row)
			break

#After being ran the information grabbed in the query is appended to the combatant list
#as a object list in order to be used by the template properly
@app.route('/combatant')
def combatants():
	cur.execute('''select * from combatant order by combatant.name ASC;''')
	result = list(cur.fetchall())
	combatant = []
	for i in result:
		combatant.append(Object(i[0], i[1], i[2]))
	return render_template('combatants.html', combatants=combatant)

#After being ran allows results to be displayed to the screen in descending order based on most wins
#derived from Samules
@app.route('/results')
def results():
	fighters = []

	cur.execute('''select count(*) from combatant;''')
	
	num_of_combatants = cur.fetchone()[0]
	
	for i in range(1, num_of_combatants+1):
		total_wins = 0
		cur.execute("select * from fight where combatant_one = %s and winner = 'One'", (i,))
		total_wins = len(cur.fetchall())
		cur.execute("select * from fight where combatant_two = %s and winner = 'Two'", (i,))
		total_wins+= len(cur.fetchall())
		cur.execute("SELECT name FROM combatant WHERE id=%s", (i,))
		combatant_name = cur.fetchone()[0]
		fighters.append( [i, total_wins, combatant_name] )

	fighters.sort(key=lambda x: x[1], reverse=True)
	for i in range(len(fighters)):
		fighters[i].append(i+1)

	return(render_template('results.html',combatants=fighters))

#After being ran allows battle.html to be displayed to the screen with the information
#grabbed by the query, which was appended into a list called fight and then sent to the 
#template in order to be used properly
@app.route('/battle')
def battle():
	cur.execute('''select combatant_one, combatant_two, 
				(select name from combatant where combatant_one = combatant.id),
				(select name from combatant where combatant_two = combatant.id),
				winner, fight.id from fight, combatant;''')
	result = list(cur.fetchall())
	fight = []
	for i in result:
		fight.append(Fight(i[0], i[1], i[2], i[3], i[4], i[5]))
	return render_template('battle.html', fights=fight)

#After being ran allows information to be displayed to the screen based on the 
#results of the fight between the first and second id
@app.route('/battle/<id1>-<id2>')
def battle_details(id1=None, id2=None):
	cur.execute(''' select * from fight;''')

	result = list(cur.fetchall())
	
	fight = []	

	for i in result:
		if i[0] == int(id1) and i[1] == int(id2):
			return render_template('battle_details.html', details=i) 
			break
		elif i[1] == int(id1) and i[0] == int(id2):
			return render_template('battle_details.html', details=i) 
			break
	
if __name__ == '__main__':
	app.run(debug=True, port=8053)
